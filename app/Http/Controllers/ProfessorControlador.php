<?php

namespace App\Http\Controllers;

use App\Aluno;
use App\Materia;
use App\Professor;
use App\Nota;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\calculaMedia;

class ProfessorControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professor =  Professor::all();

        $materia = Materia::all();

        $aluno = Aluno::all();

        $nota = DB::table('notas')
                        ->select('notas.id as idnota','alunos.nome as nomealuno','materias.descricao as materiadesc','notas.n1 as n1','notas.n2 as n2')
                         ->join('alunos','notas.aluno_id','=','alunos.id')
                        ->join('materias','notas.materia_id','=','materias.id')                        
                        ->get();

        $grade = DB::table('grade_escolars')
                            ->select('grade_escolars.id as id','professors.nome as nomeprof','materias.descricao as descmat','salas.descricao as descsala','grade_escolars.turno as turno')
                            ->join('professors','grade_escolars.professor_id','=','professors.id') 
                            ->join('materias','grade_escolars.materia_id','=','materias.id')
                            ->join('salas','grade_escolars.sala_id','=','salas.id')
                            ->get();               

         
        return view('professor',compact('professor','materia','aluno','nota','grade'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $nota = new Nota();
       $nota->aluno_id = $request->input('selectAluno');

       $nota->professor_id =1; //Auth::id();
       $nota->materia_id = $request->input('selectMateria');
       $nota->n1 = $request->input('n1');
       $nota->n2 = $request->input('n2');
       $nota->save();

       return redirect('/professor');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $aluno = Aluno::all();
       $materia= Materia::all();

        $n = Nota::find($id);
        if (isset($n)){
            return view('/editarnota',compact('n','aluno','materia'));
        }
        return redirect('/gestor');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nota = Nota::find($id);
        if (isset($nota)){
            $nota->n1 = $request->input('n1'); 
            $nota->n2 = $request->input('n2'); 
                     
            $nota->save();
        }
        return redirect('/professor');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nota = Nota::find($id);
        if (isset($nota)){
            $nota->delete();
        }
        return redirect('/professor');
    }

    

    
    
}
