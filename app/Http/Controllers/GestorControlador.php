<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Sala;
use App\Materia;
use App\Professor;
use App\Aluno;
use App\GradeEscolar;
use Illuminate\Support\Facades\DB;
use App\usuarios;
use Illuminate\Support\Facades\Hash;




class GestorControlador extends Controller
{
   
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sala =  Sala::all();

        $materia =  Materia::all();

        $professor = Professor::all();

        $grade = DB::table('grade_escolars')
                        ->select('grade_escolars.id as id','grade_escolars.turno as turno',
                        'professors.nome as nomeprof','materias.descricao as descmat',
                        'salas.descricao as descsala')                       
                        ->join('professors','grade_escolars.professor_id','=','professors.id')
                        ->join('materias','grade_escolars.materia_id','=','materias.id')
                        ->join('salas','grade_escolars.sala_id','=','salas.id')
                        ->get();

        
        $aluno = DB::table('alunos')
                         ->select('alunos.id as idaluno','alunos.nome as nome','salas.descricao as descricao')
                         ->join('salas','alunos.sala_id','=','salas.id')                         
                         ->get();
        
        return view('gestor',compact('materia','sala','professor','aluno','grade'));




    }
    public function indexSala()
    {
       // $sala =  Sala::all();
       // return view('gestor',compact('sala'));
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $grad = new GradeEscolar();
        $grad->professor_id = $request->input('selectProfessor');

                    if ($request->input('matutino') != ''){
                        $grad->turno = 'Matutino';              
                    }
                    else if($request->input('vespertino') != ''){
                    $grad->turno = 'Vespertino';
                    }
                    else{
                        $grad->turno = 'Noturno';   
                    }  

        $grad->materia_id = $request->input('selectMateria');
        $grad->sala_id = $request->input('selectSala');     
        $grad->save();   

        return redirect('/gestor');

    }
    
    public function storeSala(Request $request)
    {
        $sala = new Sala();
        $sala->descricao = $request->input('nomesala');   /* laravel.com/docs/5.6/validation exemplos de validacoes */
        $sala->save();

        return redirect('/gestor');
        //echo '$(\'.nav-tabs a[href="#sala"]\').tab(\'show\')';
    }

    public function storeMateria(Request $request)
    {

        $request->validate([
            'nomemateria' => 'required|min:3|max:255',            
        ]);

        $materia = new Materia();
        $materia->descricao = $request->input('nomemateria');   
        $materia->save();

        return redirect('/gestor');

    }

    public function storeProfessor(Request $request)
    {
        $professor = new Professor();
        $professor->nome = $request->input('nomeprofessor');        
        $professor->save();

         $usuario = new usuarios();

         if ($request->input('senha') == $request->input('confirmaSenha')){
        $usuario->name = $request->input('nomeprofessor');
         $usuario->email = $request->input('email');
         $usuario->password = Hash::make($request->input('senha'));
         $usuario->perfil = 2; 
         $usuario->save();
        }else {
            return "A confirmacao da senha falhou";
        }

        return redirect('/gestor');
    }

    public function storeAluno(Request $request)
    {
        $aluno = new Aluno();
        $aluno->nome = $request->input('nomealuno');
        $aluno->sala_id = $request->input('selectSala');
        $aluno->save();

        $usuario = new usuarios();
        if ($request->input('senha') == $request->input('confirmaSenha')){
        $usuario->name = $request->input('nomealuno');
        $usuario->email = $request->input('email');
        $usuario->password = Hash::make($request->input('senha'));
        $usuario->perfil = 3; 
        $usuario->save();
       }else {
           return "A confirmacao da senha falhou";      
       }

        return redirect('/gestor');    
    }  

    public function storeUsuario(Request $request){
        $usuario = new usuarios();
        
        if ($request->input('senha') == $request->input('confirmaSenha')){
        $usuario->name = $request->input('nomegestor');
        $usuario->email = $request->input('email');
        $usuario->password = Hash::make($request->input('senha'));
        $usuario->perfil = 1; 
        $usuario->save();
       }else {
           return "A confirmacao da senha falhou";      
       }

        return redirect('/gestor');    
   

    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $prof = Professor::all();
        $mat = Materia::all();
        $sala = Sala::all();

        $grad = GradeEscolar::find($id);
        if (isset($grad)){
            return view('/editargrade',compact('grad','prof','mat','sala'));
        }
        return redirect('/gestor');
    } 

    public function editAluno($id)
    {
        $sala = Sala::all();

        $a = Aluno::find($id);
        if (isset($a)){
            return view('/editaraluno',compact('a','sala'));
        }
        return redirect('/gestor');


    } 
    

    public function editMateria($id)
    {
        $m = Materia::find($id);
        if (isset($m)){
            return view('/editarmateria',compact('m'));
        }
        return redirect('/gestor');
    } 
    
    public function editProf($id)
    {
        $p = Professor::find($id);
        if (isset($p)){
            return view('/editarprof',compact('p'));
        }
        return redirect('/gestor');
    } 

    public function editSala($id)
    {
        $s = Sala::find($id);
        if (isset($s)){
            return view('/editarsala',compact('s'));
        }
        return redirect('/gestor');
    } 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    public function update(Request $request, $id)
    {
        $grad = GradeEscolar::find($id);
        if (isset($grad)){
            $grad->professor_id = $request->input('selectProfessor'); 

            if ($request->input('matutino') != ''){
                $grad->turno = 'Matutino';              
            }
            else if($request->input('vespertino') != ''){
            $grad->turno = 'Vespertino';
            }
            else{
                $grad->turno = 'Noturno';   
            }  
                  
            $grad->materia_id = $request->input('selectMateria');  
            $grad->sala_id = $request->input('selectSala');  
                  
            $grad->save();
        }
        return redirect('gestor');
    }

    public function updateAluno(Request $request, $id)
    {
        $aluno = Aluno::find($id);
        if (isset($aluno)){
            $aluno->nome = $request->input('nomeAluno'); 
           // $aluno->sala_id = $request->input('selectSala');            
            $aluno->save();
        }
        return redirect('gestor');
    }
    
    

    public function updateSala(Request $request, $id)
    {
        $s = Sala::find($id);
        if (isset($s)){
            $s->descricao = $request->input('sala');            
            $s->save();
        }
        return redirect('gestor');
    }
    public function updateMateria(Request $request, $id)
    {
        $mat = Materia::find($id);
        if (isset($mat)){
            $mat->descricao = $request->input('materia');            
            $mat->save();
        }
        return redirect('gestor');
    }


    public function updateProf(Request $request, $id)
    {
        $prof = Professor::find($id);
        if (isset($prof)){
            $prof->nome = $request->input('nomeProf');            
            $prof->save();
        }
        return redirect('gestor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grade = GradeEscolar::find($id);
        if (isset($grade)){
            $grade->delete();
        }
        return redirect('/gestor');     
    }

    public function destroySala($id)
    {
        $sala = Sala::find($id);
        if (isset($sala)){
            $sala->delete();
        }
        return redirect('/gestor');       
    }

    public function destroyMateria($id)
    {  
        $mat = Materia::find($id);
        if (isset($mat)){
            $mat->delete();
        }
        return redirect('/gestor');       
    }

    public function destroyAluno($id) 
    {  
        $a = Aluno::find($id);
        if (isset($a)){
            $a->delete();
        }
        return redirect('/gestor');       
    }

    public function destroyProf($id)    
    {  
        $prof = Professor::find($id);
        if (isset($prof)){
            $prof->delete();
        }
        return redirect('/gestor');       
    }

    
}
