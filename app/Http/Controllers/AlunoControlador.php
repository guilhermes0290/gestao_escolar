<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlunoControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nota = DB::table('notas')
                        ->select('notas.id as idnota','professors.nome as nomeprof','materias.descricao as materiadesc','notas.n1 as n1','notas.n2 as n2')
                         ->join('professors','notas.professor_id','=','professors.id')
                        ->join('materias','notas.materia_id','=','materias.id')                        
                        ->get();

                        return view('aluno',compact('nota'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $regras = [
            'nome'=>'required|min:5|max:10|unique:clientes',
            'email'=>'required|email'   
            
            ];
            $mensagens = [
                'required'=>'o Atributo :attribute nao pode estar em branco',  // exemplo de msg generica pegando o nome do campo com o attribute
                'nome.required'=> 'O nome é requerido',
                'nome.min'=> 'É necessario no minimo 3 caracters no nome',
                'email.required'=> 'digite um endereco de email',
                'email.email'=> 'Digite um endereco de email valido'
            ];
            $request->validate($regras,$mensagens);

    $aluno = new Aluno();
        $aluno->nome = $request->input('nome');   /* laravel.com/docs/5.6/validation exemplos de validacoes */
        $aluno->sala_id = $request->input('sala_id');
        
        $aluno->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
