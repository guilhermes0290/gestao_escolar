<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('newlogin');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/aluno','AlunoControlador@index');




Route::get('/professor', function(){
    return view('professor');
});

 /* Route::get('/gestor', function(){
    return view('gestor');
}); */

Route::get('/gestor', 'GestorControlador@index');

Route::get('/admin/login', 'Auth\AdminLoginController@index')->name('admin.login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
//sala gestor
 Route::post('/sala', 'GestorControlador@storeSala');  
 Route::post('/sala/{id}', 'GestorControlador@updateSala'); 
 Route::get('/sala/editar/{id}', 'GestorControlador@editSala');  

 Route::post('/materia', 'GestorControlador@storeMateria'); 
 Route::post('/prof', 'GestorControlador@storeProfessor');    

 Route::post('/gradeescolar', 'GestorControlador@store'); 
//Materia gestor
 Route::get('materia/editar/{id}', 'GestorControlador@editMateria'); 
 Route::get('materia/apagar/{id}', 'GestorControlador@destroyMateria'); 
 Route::post('materia/{id}', 'GestorControlador@updateMateria'); 


//aluno gestor
 Route::post('aluno/{id}', 'GestorControlador@updateAluno');
 Route::get('aluno/apagar/{id}', 'GestorControlador@destroyAluno');
 Route::post('/aluno/novo', 'GestorControlador@storeAluno');  
 Route::get('aluno/editar/{id}', 'GestorControlador@editAluno');

 Route::post('/usuario', 'GestorControlador@storeUsuario'); 

//grade
Route::post('gradeescolar/{id}', 'GestorControlador@update'); 
 Route::get('sala/apagar/{id}', 'GestorControlador@destroySala'); 
 Route::get('gradeescolar/editar/{id}', 'GestorControlador@edit'); 
 Route::get('gradeescolar/apagar/{id}', 'GestorControlador@destroy');      
 Route::get('nota/apagar/{id}', 'ProfessorControlador@destroy');

// professor  gestor
 Route::post('prof/{id}', 'GestorControlador@updateProf');
 Route::get('prof/apagar/{id}', 'GestorControlador@destroyProf');   
 Route::get('prof/editar/{id}', 'GestorControlador@editProf'); 
 Route::get('/professor', 'ProfessorControlador@index'); 
 Route::post('/professor/novo', 'ProfessorControlador@store'); 

 Route::get('/nota/editar/{id}', 'ProfessorControlador@edit'); 
 Route::post('/nota/{id}', 'ProfessorControlador@update'); 
 
 

 //login

 Route::get('/newlogin', 'validaLogin@index'); 
 Route::post('/validaLogin', 'validaLogin@store'); 




 

