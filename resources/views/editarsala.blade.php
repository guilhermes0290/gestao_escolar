@extends('layouts.modelo')
@section('body')
    
    <div class="card border">
        <h5 class="card-title">Edição da Sala</h5>
        <div class="card-body">
        <form action="/sala/{{$s->id}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="novaMateria">Descricao da sala</label>
                <input type="text" class="form-control" name="sala" value="{{$s->descricao}}"
                    id="sala" placeholder="Nome da Sala" required>                    
                </div>
                <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                <button type="cancel" class="btn btn-danger btn-sm">Cancelar</button>
            </form>
        </div>
    </div>
@endsection