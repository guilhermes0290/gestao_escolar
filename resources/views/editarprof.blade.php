@extends('layouts.modelo')
@section('body')
    
    <div class="card border">
        <h5 class="card-title">Edição de Professor</h5>
        <div class="card-body">
        <form action="/prof/{{$p->id}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="novaCategoria">Nova Categoria</label>
                <input type="text" class="form-control" name="nomeProf" value="{{$p->nome}}"
                    id="nomeProf" placeholder="Nome Professor" required>                    
                </div>
                <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                <button type="cancel" class="btn btn-danger btn-sm">Cancelar</button>
            </form>
        </div>
    </div>
@endsection