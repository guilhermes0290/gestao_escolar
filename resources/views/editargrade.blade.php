@extends('layouts.modelo')
@section('body')
    
    <div class="card border">
        <h5 class="card-title">Edição de Professor</h5>
        <div class="card-body">
        <form action="/gradeescolar/{{$grad->id}}" method="POST">
                @csrf
                <div class="form-group col-3">
                    <label for="materia">Selecione o Professor</label>
                    <select class="form-control" name="selectProfessor" id="selectProfessor">    
                      @foreach ($prof as $p)
                    <option value="{{$p->id}}">{{$p->nome}}</option> 
                      @endforeach
                                              
                    </select>
                  </div>
                  <div class="col-6">
                    <label for="turno">Informe o Turno</label>
                   <fieldset id="situacao">
                  
                   
                   <div class="form-row" id="situacaoS">
                  
                    <div class="form-inline  col-3">
                      
                         <input class="checkgroup" type="checkbox" name="matutino"  value="matutino" id="matutino" ><label for="cativo" >&nbsp;Matutino</label><br />
                    </div>
                  
                    <div class="form-inline  col-3">
                         <input class="checkgroup" type="checkbox" name="vespertino" value="vespertino" id="vespertino"><label for="cinativo" >&nbsp;Vespestino</label>
                    </div>
                  
                    <div class="form-inline col-3">
                      <input class="checkgroup" type="checkbox" name="noturno" value="noturno" id="noturno"><label for="cinativo">&nbsp;Noturno</label>
                    </div>
                  </div>
                  </fieldset>
                  </div><p>
                    <div class="form-group col-3">
                        <label for="materia">Selecione a Matéria</label>
                        <select class="form-control" name="selectMateria" id="selectMateria">    
                          @foreach ($mat as $m)
                        <option value="{{$m->id}}">{{$m->descricao}}</option> 
                          @endforeach
                                                  
                        </select>
                      </div>

                      <div class="form-group col-3">
                        <label for="materia">Selecione a Sala de Aula</label>
                        <select class="form-control" id="selectSala" name="selectSala">
                          @foreach ($sala as $s)
                               <option value="{{$s->id}}">{{$s->descricao}}</option>
                          @endforeach
                         
                                                               
                        </select>
                      </div>

                <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                <button type="cancel" class="btn btn-danger btn-sm">Cancelar</button>
            </form>
        </div>
    </div>
@endsection