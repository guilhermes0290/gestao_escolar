@extends('layouts.modelo')

@section('body')
<p>
  <div class="form-row col-12">   
    <div class="form-group col">
      <h5>Portal do Aluno</h5>
  </div>
      
      <div class="form-group col">
        <a href="/newlogin"class="btn btn-sm btn-danger">Sair</a>
    </div>
  </div>
 <p>
 

 <div class="card border">
    <div class="card-body">
        <h5 class="card-title">Painel de Notas Bimestrais</h5>
        
        <table class="table table-ordered table-hover " id="tabelaNotas">
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Professor</th>
                    <th>Materia</th>
                    <th>N1</th>
                    <th>N2</th>
                    <th>Média Final</th>
                    <th>Situação</th>                   
                </tr>
            </thead>
            <tbody>

              @if (count($nota)>0)
              @foreach ($nota as $n)

              <tr>
                  <td>{{$n->idnota}} </td>
                  <td>{{$n->nomeprof}}</td>
                  <td>{{$n->materiadesc}}</td>
                  <td>{{$n->n1}}</td>
                  <td>{{$n->n2}}</td>                          
                  <td>{{($n->n1+$n->n2) /2}}</td>

                  @if (( ($n->n1+$n->n2) /2) < 5)
                    <td>{{'REPROVADO'}}</td>
                  
                  @elseif(( ($n->n1+$n->n2) /2) >= 7)
                    <td>{{'APROVADO'}}</td>
                 
                  @else
                    <td>{{'EXAME'}}</td>                  
                  @endif
                    
              </tr>
             
                  
              @endforeach
              @endif

            </tbody>
        </table>
    </div>
         
  </div> 
    
    
@endsection