@extends('layouts.modelo')
@section('body')
    
    <div class="card border">
        <h5 class="card-title">Edição de Professor</h5>
        <div class="card-body">
        <form action="/nota/{{$n->id}}" method="POST">
                @csrf
                <div class="form-row col-6 offset-6">
                    <label for="materia">Selecione o Aluno</label>
                    <select class="form-control" id="selectAluno" name="selectAluno" disabled>
                      @foreach ($aluno as $a)
                    <option value="{{$a->id}}">{{$a->nome}}</option>
                      @endforeach                                         
                    </select>
                  </div>
                  <div class="form-row col-6">
                    <label for="materia">Selecione a Materia</label>
                    <select class="form-control" id="selectMateria" name="selectMateria" disabled>
                     @foreach ($materia as $m)
                    <option value="{{$m->id}}">{{$m->descricao}}</option>
                     @endforeach                 
                                                    
                    </select>
                    
                  </div>
                  <p>
                  <div class="form-row">
                
                    <div class="form-row-inline col-3">
                      <label for="Materia">N1</label>
                    <input type="number" class="form-control" id="descMateria"  name="n1" value="{{$n->n1}}" placeholder=""required >
                    </div>
      
                    <div class="form-row-inline col-3">
                      <label for="Materia">N2</label>
                      <input type="number" class="form-control" id="descMateria" value="{{$n->n2}}" name="n2" placeholder="" required>
                    </div>
                    
                  </div>
                  

                <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                <button type="cancel" class="btn btn-danger btn-sm">Cancelar</button>
            </form>
        </div>
    </div>
@endsection