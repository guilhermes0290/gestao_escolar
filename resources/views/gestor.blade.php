@extends('layouts.modelo');
    @section('body')
    <div class="form-row">   
      <div class="form-group col">
        <h5>Controle do gestor escolar</h5>
    </div>
        
        <div class="form-group col">
          <a href="/newlogin"class="btn btn-sm btn-danger">Sair</a>
      </div>
    </div>
        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">       
               <a class="nav-item nav-link active" id="nav-home-tab1" data-toggle="tab" href="#professor" role="tab" aria-controls="nav-home" aria-selected="true">Professor</a>        
               <a class="nav-item nav-link" id="nav-historico-tab2" data-toggle="tab" href="#aluno" role="tab" aria-controls="nav-historico" aria-selected="true">Aluno</a>
               <a class="nav-item nav-link" id="nav-historico-tab3" data-toggle="tab" href="#materia" role="tab" aria-controls="nav-historico" aria-selected="true">Materia</a>
               <a class="nav-item nav-link" id="nav-historico-ta4" data-toggle="tab" href="#sala" role="tab" aria-controls="nav-historico" aria-selected="true">Sala</a>
               <a class="nav-item nav-link" id="nav-historico-ta4" data-toggle="tab" href="#gradeEscolar" role="tab" aria-controls="nav-historico" aria-selected="true">Grade Escolar</a>
               <a class="nav-item nav-link" id="nav-historico-ta4" data-toggle="tab" href="#gestor" role="tab" aria-controls="nav-historico" aria-selected="true">Cadastro de Gestores</a>
          </div>
       </nav>

       <div class="tab-content" id="myTabContent">

        <div class="tab-pane fade show active" id="professor" role="tabpanel" aria-labelledby="home-tab">
          <p>
                <form action="/prof" method="POST">
                  @csrf
                  <div class="form-group col-6">
                    <label for="nomeprofessor">Nome do Professor</label>
                    <input type="text" class="form-control" id="nomeprofessor" name="nomeprofessor" placeholder="Nome do Professor..."  required>
                  </div>
                  <div class="form-group col-4">
                    <label for="nomeprofessor">Email:</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email para Login" required>
                  </div>
                  <div class="form-group col-4">
                    <label for="nomeprofessor">Senha:</label>
                    <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha para login..." required>
                  </div>
                  <div class="form-group col-4">
                    <label for="nomeprofessor">Confirme a Senha</label>
                    <input type="password" class="form-control" id="confirmaSenha" name="confirmaSenha" placeholder="Confirme a Senha" required>
                  </div>
                 
                  <button type="submit" class="btn btn-sm btn-primary" role="button">Salvar</button>
                </form>

                <div class="card border">
                  <div class="card-body">
                      <h5 class="card-title">Lista de Professores</h5>
                      
                      <table class="table table-ordered table-hover " id="tabelaProdutos">
                          <thead>
                              <tr>
                                  <th>Codigo</th>
                                  <th>Nome Professor</th>                                                                
                              </tr>
                          </thead>
                          <tbody>

                            @if (count($professor)>0)
                            @foreach ($professor as $p)
      
                            <tr>
                                <td>{{$p->id}} </td>
                                <td>{{$p->nome}}</td>
                                <td>
                                <a href="/prof/editar/{{$p->id}}" class="btn btn-sm btn-primary">Editar</a>
                                <a href="/prof/apagar/{{$p->id}}" onclick="remover();" class="btn btn-sm btn-danger">Apagar</a>
                                </td>
                            </tr>
                           
                                
                            @endforeach
                            @endif
              
                          </tbody>
                      </table>
                  </div>
                       
                </div> 
        </div>

        <div class="tab-pane fade show" id="aluno" role="tabpanel" aria-labelledby="home-tab">
          <p>         
          <form action="/aluno/novo" method="POST">
            @csrf
                      <div class="form-group col-6">
                        <label for="nomealuno">Nome do Aluno</label>
                        <input type="text" class="form-control" id="nomealuno" name="nomealuno" placeholder="Nome do aluno...">
                      </div>
                      <div class="form-group col-3">
                        <label for="materia">Selecione a Sala</label>
                        <select  class="form-control" name="selectSala" id="selectsala">
                       @foreach ($sala as $s)
                       <option value="{{$s->id}}">{{$s->descricao}}</option>
                       @endforeach                      
                                  
                        </select>
                      </div>

                      <div class="form-group col-4">
                        <label for="nomeprofessor">Email:</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email para Login">
                      </div>
                      <div class="form-group col-4">
                        <label for="nomeprofessor">Senha:</label>
                        <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha para login...">
                      </div>
                      <div class="form-group col-4">
                        <label for="nomeprofessor">Confirme a Senha</label>
                        <input type="password" class="form-control" id="confirmaSenha" name="confirmaSenha" placeholder="Confirme a Senha">
                      </div>
                      <button type="submit"class="btn btn-sm btn-primary" role="button">Salvar</button>
                    </form>

                    <div class="card border">
                      <div class="card-body">
                          <h5 class="card-title">Lista de Alunos</h5>
                          
                          <table class="table table-ordered table-hover " id="tabelaProdutos">
                              <thead>
                                  <tr>
                                      <th>Codigo</th>
                                      <th>Nome Aluno</th>
                                      <th>Sala</th>                                      
                                  </tr>
                              </thead>
                              <tbody>
                                @if (count($aluno)>0)
                                @foreach ($aluno as $a)
          
                                <tr>
                                    <td>{{$a->idaluno}} </td>
                                    <td>{{$a->nome}}</td>
                                    <td>{{$a->descricao}}</td>
                                    <td>
                                    <a href="/aluno/editar/{{$a->idaluno}}" class="btn btn-sm btn-primary">Editar</a>
                                    <a href="/materia/apagar/{{$a->idaluno}}" onclick="remover();" class="btn btn-sm btn-danger">Apagar</a>
                                    </td>
                                </tr>
                               
                                    
                                @endforeach
                                @endif
                             
                  
                              </tbody>
                          </table>
                      </div>
                           
                    </div> 
        </div>

        <div class="tab-pane fade show" id="materia" role="tabpanel" aria-labelledby="home-tab">
        <p>
          <form action="/materia" method="POST">
            @csrf
            <div class="form-group col-6">
              <label for="nomeprofessor">Nome do Materia</label>
              <input type="text" class="form-control" id="nomemateria"name="nomemateria" placeholder="Nome da materia...">
            </div>            
            <button type="submit"class="btn btn-sm btn-primary" role="button">Salvar</button>
          </form>

          <div class="card border">
            <div class="card-body">
                <h5 class="card-title">Lista de Materias</h5>
                
                <table class="table table-ordered table-hover " id="tabelaProdutos">
                    <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Nome da Materia</th>                              
                        </tr>
                    </thead>
                    <tbody>

                      @if (count($materia)>0)
                      @foreach ($materia as $m)

                      <tr>
                          <td>{{$m->id}} </td>
                          <td>{{$m->descricao}}</td>
                          <td>
                          <a href="/materia/editar/{{$m->id}}" class="btn btn-sm btn-primary">Editar</a>
                          <a href="/materia/apagar/{{$m->id}}" onclick="remover();" class="btn btn-sm btn-danger">Apagar</a>
                          </td>
                      </tr>
                     
                          
                      @endforeach
                      @endif
        
                    </tbody>
                </table>
            </div>
                 
          </div> 

        </div>

        <div class="tab-pane fade show" id="sala" role="tabpanel" aria-labelledby="home-tab">
          <p>
            <form action="/sala" method="POST">
              @csrf
              <div class="form-group col-6">
                <label for="nomeprofessor">Descrição da sala</label>
                <input type="text" class="form-control" name="nomesala" id="nomemateria" placeholder="Descrição da sala...">
              </div>              
              <button type="submit"class="btn btn-sm btn-primary" role="button">Salvar</button>
            </form>
  
            <div class="card border">
              <div class="card-body">
                  <h5 class="card-title">Lista de Salas</h5>
                  
                  <table class="table table-ordered table-hover " id="tabelaProdutos">
                      <thead>
                          <tr>
                              <th>Codigo</th>
                              <th>Descrição da sala</th>                                  
                          </tr>
                      </thead>
                      <tbody>
                    @if (count($sala)>0)
                        @foreach ($sala as $s)

                        <tr>
                            <td>{{$s->id}} </td>
                            <td>{{$s->descricao}}</td>
                            <td>
                            <a href="/sala/editar/{{$s->id}}" class="btn btn-sm btn-primary">Editar</a>
                            <a href="/sala/apagar/{{$s->id}}" onclick="remover();"  class="btn btn-sm btn-danger">Apagar</a>
                            </td>
                        </tr>
                       
                            
                        @endforeach
                        @endif
                        
                       
          
                      </tbody>
                  </table>
                  
              </div>
                   
            </div> 
        </div>
        <div class="tab-pane fade show " id="gradeEscolar" role="tabpanel" aria-labelledby="home-tab">
              <form action="/gradeescolar" method="POST"> 
                @csrf
                <div class="form-group col-3">
                  <label for="materia">Selecione o Professor</label>
                  <select class="form-control" name="selectProfessor" id="selectProfessor">    
                    @foreach ($professor as $p)
                  <option value="{{$p->id}}">{{$p->nome}}</option> 
                    @endforeach
                                            
                  </select>
                </div>
                       
                
 <div class="col-6">
  <label for="turno">Informe o Turno</label>
 <fieldset id="situacao">

 
 <div class="form-row" id="situacaoS">

  <div class="form-inline  col-3">
    
       <input class="checkgroup" type="checkbox" name="matutino"  value="matutino" id="matutino" ><label for="cativo">&nbsp;Matutino</label><br />
  </div>

  <div class="form-inline  col-3">
       <input class="checkgroup" type="checkbox" name="vespertino" value="vespertino" id="vespertino"><label for="cinativo">&nbsp;Vespestino</label>
  </div>

  <div class="form-inline col-3">
    <input class="checkgroup" type="checkbox" name="noturno" value="noturno" id="noturno"><label for="cinativo">&nbsp;Noturno</label>
  </div>
</div>
</fieldset>
</div><p>
                <div class="form-group col-3">
                  <label for="materia">Selecione a Matéria</label>
                  <select class="form-control" name="selectMateria" id="selectMateria">    
                    @foreach ($materia as $m)
                  <option value="{{$m->id}}">{{$m->descricao}}</option> 
                    @endforeach
                                            
                  </select>
                </div>

                <div class="form-group col-3">
                  <label for="materia">Selecione a Sala de Aula</label>
                  <select class="form-control" id="selectturno" name="selectSala">
                    @foreach ($sala as $s)
                         <option value="{{$s->id}}">{{$s->descricao}}</option>
                    @endforeach
                   
                                                         
                  </select>
                </div>
                <button type="submit"class="btn btn-sm btn-primary" role="button">Salvar</button>
              </form> 

              <div class="card border">
                <div class="card-body">
                    <h5 class="card-title">Grade Escolar</h5>
                    
                    <table class="table table-ordered table-hover " id="tabelaGrade">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Nome do Professor</th>
                                <th>Turno</th>
                                <th>Materia</th>
                                <th>Sala</th>     
                            </tr>
                        </thead>
                        <tbody>
                          
                          @if (count($grade)>0)
                          @foreach ($grade as $g)
  
                          <tr>
                              <td>{{$g->id}} </td>
                              <td>{{$g->nomeprof}} </td>
                              <td>{{$g->turno}} </td>
                              <td>{{$g->descmat}} </td>
                              <td>{{$g->descsala}} </td>

                              <td>
                              <a href="/gradeescolar/editar/{{$g->id}}"  class="btn btn-sm btn-primary">Editar</a>
                              <a href="/gradeescolar/apagar/{{$g->id}}" onclick="removerGrade()" class="btn btn-sm btn-danger">Apagar</a>
                              </td>
                          </tr>
                         
                              
                          @endforeach
                          @endif
            
                        </tbody>
                    </table>
                </div>
                     
              </div> 
        </div>
        
        <div class="tab-pane fade show " id="gestor" role="tabpanel" aria-labelledby="home-tab">
          <p>
                <form action="/usuario" method="POST">
                  @csrf
                  <div class="form-group col-6">
                    <label for="nomeprofessor">Nome do Gestor</label>
                    <input type="text" class="form-control" id="nomeprofessor" name="nomegestor" placeholder="Nome do Professor...">
                  </div>
                  <div class="form-group col-4">
                    <label for="nomeprofessor">Email:</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email para Login">
                  </div>
                  <div class="form-group col-4">
                    <label for="nomeprofessor">Senha:</label>
                    <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha para login...">
                  </div>
                  <div class="form-group col-4">
                    <label for="nomeprofessor">Confirme a Senha</label>
                    <input type="password" class="form-control" id="confirmaSenha" name="confirmaSenha" placeholder="Confirme a Senha">
                  </div>
                 
                  <button type="submit" class="btn btn-sm btn-primary" role="button">Salvar</button>
                </form>

               
        </div>


       </div>

      
    
    @endsection
