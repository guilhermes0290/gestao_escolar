<nav class="navbar navbar-expand-md navbar-light bg-light rounded">
    <a class="navbar-brand pb-2" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cadastro</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="/clientes">Clientes</a></li>
                                <li><a class="dropdown-item" href="/fornecedores">Fornecedores</a></li>
                                <li><a class="dropdown-item" href="/itens">Itens</a></li>
                                <li><a class="dropdown-item" href="/func">Funcionários</a></li>
                                <li><a class="dropdown-item" href="/planos">Planos</a></li>
                         </ul>
                </li>
                <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Lançamentos</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="/cr">Contas a Receber</a></li>
                                <li><a class="dropdown-item" href="/cp">Contas a Pagar</a></li>
                                <li><a class="dropdown-item" href="/os">Ordem de Serviço</a></li>                                     
                         </ul>
                </li>
                <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Relatórios</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="#">Relatorio 1</a></li>
                                <li><a class="dropdown-item" href="#">Relatorio 2</a></li>
                                <li><a class="dropdown-item" href="#">Relatorio 3</a></li>                                     
                         </ul>
                </li>
               <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Configurações</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="/usuarios">Usuários</a></li>
                                <li><a class="dropdown-item" href="/parametros">Parametros</a></li>                                                                             
                         </ul>
                </li>
            
<!--                         <li class="nav-item dropdown"> -->
<!--                             <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Dropdown </a> -->
<!--                             <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> -->
<!--                                 <li><a class="dropdown-item" href="#">Cadastro</a></li> -->
<!--                                 <li><a class="dropdown-item" href="">qualquer coisa</a></li> -->
                                                     
<!--                                 <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="http://google.com">Google</a> -->
<!--                                     <ul class="dropdown-menu"> -->
<!--                                         <li><a class="dropdown-item" href="#">Submenu</a></li> -->
<!--                                         <li><a class="dropdown-item" href="#">Submenu0</a></li> -->
    <!--                             <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Submenu 1</a> -->
    <!--                                 <ul class="dropdown-menu"> -->
    <!--                                     <li><a class="dropdown-item" href="#">Subsubmenu1</a></li> -->
    <!--                                     <li><a class="dropdown-item" href="#">Subsubmenu1</a></li> -->
    <!--                                 </ul> -->
    <!--                             </li> -->
    <!--                             <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Submenu 2</a> -->
    <!--                                 <ul class="dropdown-menu"> -->
    <!--                                     <li><a class="dropdown-item" href="#">Subsubmenu2</a></li> -->
    <!--                                     <li><a class="dropdown-item" href="#">Subsubmenu2</a></li> -->
    <!--                                 </ul> -->
    <!--                             </li> -->
                         
<!--                                     </ul> -->
<!--                                 </li> -->
<!--                                  <li><a class="dropdown-item" href="">qualquer coisa</a></li> -->
                    </ul>
               
            </li>
            </nav>