@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="/validaLogin">
                        @csrf

                        <div class="form-row">
                            <div class="form-group col">
                                <a href="/professor"class="btn btn-sm btn-primary">Acesso do Professor</a>
                            </div>
                            <div class="form-group col">
                                <a href="/aluno"class="btn btn-sm btn-primary">Acesso do Aluno</a>
                            </div>
                            <div class="form-group col">
                                <a href="/gestor" class="btn btn-sm btn-primary">Acesso do Gestor</a>
                            </div>
                            

                        </div>

                     

                       

                        

                     
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
