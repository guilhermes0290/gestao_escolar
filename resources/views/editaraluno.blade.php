@extends('layouts.modelo')
@section('body')
    
    <div class="card border">
        <h5 class="card-title">Edição de Aluno</h5>
        <div class="card-body">
        <form action="/aluno/{{$a->id}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="novaCategoria">Alteração aluno</label>
                <input type="text" class="form-control" name="nomeAluno" value="{{$a->nome}}"
                    id="nomeAluno" placeholder="Nome do Aluno" required>                    
                </div>
                <div class="form-group col-3">
                    <label for="materia">Selecione a Sala</label>
                    <select  class="form-control" name="selectSala" id="selectsala" disabled>
                   @foreach ($sala as $s)
                   <option value="{{$s->id}}">{{$s->descricao}}</option>
                   @endforeach                      
                              
                    </select>
                  </div>
                <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                <button type="cancel" class="btn btn-danger btn-sm">Cancelar</button>
            </form>
        </div>
    </div>
@endsection