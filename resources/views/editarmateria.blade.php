@extends('layouts.modelo')
@section('body')
    
    <div class="card border">
        <h5 class="card-title">Edição da Materia</h5>
        <div class="card-body">
        <form action="/materia/{{$m->id}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="novaMateria">Nova Categoria</label>
                <input type="text" class="form-control" name="materia" value="{{$m->descricao}}"
                    id="nomeProf" placeholder="Nome da Materia" required>                    
                </div>
                <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                <button type="cancel" class="btn btn-danger btn-sm">Cancelar</button>
            </form>
        </div>
    </div>
@endsection