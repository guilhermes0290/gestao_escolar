@extends('layouts.modelo')

@section('body')
<p>
  <div class="form-row col-12">   
    <div class="form-group col">
      <h5>Controle do Professor</h5>
  </div>
      
      <div class="form-group col">
        <a href="/newlogin"class="btn btn-sm btn-danger">Sair</a>
    </div>
  </div>

<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">       
         <a class="nav-item nav-link active" id="nav-home-tab1" data-toggle="tab" href="#lancamento" role="tab" aria-controls="nav-lancamento" aria-selected="true">Lançamento de Notas</a>        
         <a class="nav-item nav-link" id="nav-historico-tab2" data-toggle="tab" href="#visualizacao" role="tab" aria-controls="nav-visualizacao" aria-selected="true">Visualizar Grade</a>
    </div>
 </nav>

 <div class="tab-content" id="myTabContent">

    <div class="tab-pane fade show active" id="lancamento" role="tabpanel" aria-labelledby="home-tab">
    <form action="/professor/novo" method="POST">
      @csrf
        <p>
        <div class="form-group">
            <div class="form-row col-6 offset-6">
                <label for="materia">Selecione o Aluno</label>
                <select class="form-control" id="selectAluno" name="selectAluno">
                  @foreach ($aluno as $a)
                <option value="{{$a->id}}">{{$a->nome}}</option>
                  @endforeach
                 
                                     
                </select>
              </div>
              <div class="form-row col-6">
                <label for="materia">Selecione a Materia</label>
                <select class="form-control" id="selectMateria" name="selectMateria">
                 @foreach ($materia as $m)
                <option value="{{$m->id}}">{{$m->descricao}}</option>
                 @endforeach                 
                                                
                </select>
                
              </div>
              <p>
              <div class="form-row">
                
              <div class="form-row-inline col-3">
                <label for="Materia">N1</label>
                <input type="number" class="form-control" id="descMateria" name="n1" placeholder="" required>
              </div>

              <div class="form-row-inline col-3">
                <label for="Materia">N2</label>
                <input type="number" class="form-control" id="descMateria" name="n2" placeholder="" required>
              </div>
            </div>
        </div>
        <div class="espaco">
          <button type="submit"class="btn btn-sm btn-primary" role="button">Salvar</button>
        </div>
        
         </form>
         

         <div class="card border">
            <div class="card-body">
                <h5 class="card-title">Painel de Notas Bimestrais</h5>
                
                <table class="table table-ordered table-hover " id="tabelaNotas">
                    <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Aluno</th>
                            <th>Materia</th>
                            <th>N1</th>
                            <th>N2</th>
                            <th>Média Final</th>                            
                        </tr>
                    </thead>
                    <tbody>

                      @if (count($nota)>0)
                      @foreach ($nota as $n)

                      <tr>
                          <td>{{$n->idnota}} </td>
                          <td>{{$n->nomealuno}}</td>
                          <td>{{$n->materiadesc}}</td>
                          <td>{{$n->n1}}</td>
                          <td>{{$n->n2}}</td>                          
                          <td>{{($n->n1+$n->n2) /2}}</td>

                         {{--  <td>{{$n->descricao}}</td> --}}
                          <td>
                          <a href="/nota/editar/{{$n->idnota}}"  class="btn btn-sm btn-primary">Editar</a>
                          <a href="/nota/apagar/{{$n->idnota}}"  onclick="remover()" class="btn btn-sm btn-danger">Apagar</a>
                          </td>
                      </tr>
                     
                          
                      @endforeach
                      @endif
        
                    </tbody>
                </table>
            </div>
                 
          </div> 

    </div>   
  <div class="tab-pane fade show" id="visualizacao" role="tabpanel" aria-labelledby="home-tab">
    <div class="card border">
        <div class="card-body">
            <h5 class="card-title">Painel de Grade</h5>
            
            <table class="table table-ordered table-hover " id="tabelaGrade">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Professor</th>
                        <th>Materia</th>
                        <th>Sala</th>
                        <th>Periodo</th>                        
                    </tr>
                </thead>
                <tbody>

                  @if (count($grade)>0)
                  @foreach ($grade as $g)

                  <tr>
                      <td>{{$g->id}} </td>
                      <td>{{$g->nomeprof}}</td>
                      <td>{{$g->descmat}}</td>
                      <td>{{$g->descsala}}</td>
                      <td>{{$g->turno}}</td>                          
                      
                      
                  </tr>
                 
                      
                  @endforeach
                  @endif
    
                </tbody>
            </table>
        </div>
             
      </div> 
</div>

</div>

@endsection